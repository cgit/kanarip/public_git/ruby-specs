Name:               ruby
Version:            1.9.1
Release:            3%{?dist}
License:            Ruby or GPLv2
URL:                http://www.ruby-lang.org/
BuildRoot:          %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Summary:            The latest and greatest version of Ruby
Group:              Development/Languages
BuildArch:          noarch
Requires(pre):      %{name}-%{version}

%description
Ruby is the interpreted scripting language for quick and easy
object-oriented programming.  It has many features to process text
files and to do system management tasks (as in Perl).  It is simple,
straight-forward, and extensible.

This package enables you to get the latest version of Ruby, the default
stack for Fedora and use multiple stacks simultaneously using the
alternatives system.

%package devel
Summary:            Development headers for Ruby
Group:              Development/Languages
Requires:           %{name}
Requires:           ruby(devel) = %{version}

%description devel
This package enables you to get the latest version of Ruby headers,
the default stack for Fedora and use multiple stacks
simultaneously using the alternatives system.

%prep

%build

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}

%clean
rm -rf %{buildroot}

%files
%defattr(-, root, root, -)

%files devel
%defattr(-, root, root, -)

%changelog
* Mon Dec 28 2009 Jeroen van Meeuwen <j.van.meeuwen@ogd.nl> - 1.9.1-3
- Second package, haha

* Mon Dec 21 2009 Jeroen van Meeuwen <j.van.meeuwen@ogd.nl> - 1.9.1-1
- First package
