# The following variables set:
#
# The Major.Minor version
%global rubymmver       1.8
# The Major.Minor.Teeny version
%global rubymmtver      1.8.6
# The patchlevel
%global _patchlevel     388

##
## Safety net for missing /etc/rpm/macros.ruby
##
%{!?ruby_sitelib: %global ruby_sitelib %{_prefix}/local/share/ruby/}
%{!?ruby_sitearch: %global ruby_sitearch %{_prefix}/local/%{_lib}/ruby/}
%{!?ruby_vendorlib: %global ruby_vendorlib %{_datadir}/ruby/}
%{!?ruby_vendorarch: %global ruby_vendorarch %{_libdir}/ruby/}

%{!?ruby_sitelib_186: %global ruby_sitelib_186 %{_prefix}/local/share/ruby/1.8.6/}
%{!?ruby_sitearch_186: %global ruby_sitearch_186 %{_prefix}/local/%{_lib}/ruby/1.8.6/}
%{!?ruby_vendorlib_186: %global ruby_vendorlib_186 %{_datadir}/ruby/1.8.6/}
%{!?ruby_vendorarch_186: %global ruby_vendorarch_186 %{_libdir}/ruby/1.8.6/}

# Work on some of the defined variables to get the things we
# want (need)
%global dotpatchlevel   %{?_patchlevel:.%{_patchlevel}}
%global patchlevel      %{?_patchlevel:-p%{_patchlevel}}
%global arcver          %{rubymmtver}%{?patchlevel}

%global rubyfullver     1.8.6
%global rubyabiver      1.8.6

%global _compatsuffix   %{?rubymmtver:-%{rubymmtver}}

%global _normalized_cpu %(echo `echo %{_target_cpu} | sed 's/^ppc/powerpc/'`)

Name:               ruby%{?_compatsuffix}

Version:            %{_patchlevel}

Release:            1%{?dist}
License:            Ruby or GPLv2
URL:                http://www.ruby-lang.org/
BuildRoot:          %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:      autoconf
BuildRequires:      bison
BuildRequires:      byacc
BuildRequires:      db4-devel
BuildRequires:      emacs
BuildRequires:      gdbm-devel
BuildRequires:      glibc-devel
BuildRequires:      libX11-devel
BuildRequires:      ncurses-devel
BuildRequires:      openssl-devel
%if 0%{?fedora} >= 12
BuildRequires:      compat-readline5-devel
%else
BuildRequires:      readline-devel
%endif
BuildRequires:      unzip
BuildRequires:      tcl-devel
BuildRequires:      tk-devel

Source0:            ftp://ftp.ruby-lang.org/pub/%{name}/%{rubymmver}/ruby-%{arcver}.tar.gz

# These patches actually make sense
Patch0:             ruby-1.8.6-p383-openssl-1.0.patch
Patch1:             ruby-1.8.6-p383-always-use-i386.patch
Patch2:             ruby-1.8.6-p383-mmt-searchpath.patch

# Weird patches
Patch101:           ruby-deadcode.patch
Patch102:           ruby-1.8.6-p383-rubyprefix.patch
Patch103:           ruby-deprecated-sitelib-search-path.patch
Patch104:           ruby-deprecated-search-path.patch
Patch105:           ruby-multilib.patch

# Again these patches make sense
Patch201:           ruby-1.8.6-p287-CVE-2008-5189.patch
Patch202:           ruby-1.8.6-p287-remove-ssl-rand-range.patch
Patch203:           ruby-1.8.6-p369-ri-gem_multipath.patch
Patch204:           ruby-1.8.6-p383-head-irb-save-history.patch
Patch205:           ruby-1.8.6-p383-mkmf-use-shared.patch

Summary:            An interpreter of object-oriented scripting language
Group:              Development/Languages
Requires:           %{name}-libs = %{version}-%{release}
Requires(post):     %{_sbindir}/alternatives
Requires(postun):   %{_sbindir}/alternatives
Provides:           /usr/bin/ruby

%description
Ruby is the interpreted scripting language for quick and easy
object-oriented programming.  It has many features to process text
files and to do system management tasks (as in Perl).  It is simple,
straight-forward, and extensible.

##
## ruby-devel
##
%package devel
Summary:    A Ruby development environment
Group:      Development/Languages
Requires:   %{name}-libs = %{version}-%{release}
Provides:   ruby(devel) = %{rubymmver}
Provides:   ruby(devel) = %{rubymmtver}

%description devel
Header files and libraries for building a extension library for the
Ruby or an application embedded Ruby.

##
## ruby-irb
##
%package irb
Summary:    The Interactive Ruby
Group:      Development/Languages
Requires:   %{name} = %{version}-%{release}
Provides:   ruby(irb) = %{rubymmver}
Provides:   ruby(irb) = %{rubymmtver}

%description irb
The irb is acronym for Interactive Ruby.  It evaluates ruby expression
from the terminal.

##
## ruby-libs
##
%package libs
Summary:    Libraries necessary to run Ruby
Group:      Development/Libraries
# Always offer both the rubymmver and rubymmtver Ruby ABI
Provides:   ruby(abi) = %{rubymmver}
Provides:   ruby(abi) = %{rubymmtver}
Provides:   ruby(api) = %{rubymmver}
Provides:   ruby(api) = %{rubymmtver}

%description libs
This package includes the libruby, necessary to run Ruby.

##
## ruby-mode
##
%package mode
Summary:    Emacs Lisp ruby-mode for the scripting language Ruby
Group:      Applications/Editors
Requires:   emacs-common

%description mode
Emacs Lisp ruby-mode for the object-oriented scripting language Ruby.

##
## ruby-rdoc
##
%package rdoc
Summary:    A tool to generate documentation from Ruby source files
Group:      Development/Languages
Requires:   %{name} = %{version}-%{release}
Requires:   ruby(irb) = %{rubymmtver}
Provides:   ruby(rdoc) = %{rubymmver}
Provides:   ruby(rdoc) = %{rubymmtver}

%description rdoc
The rdoc is a tool to generate the documentation from Ruby source files.
It supports some output formats, like HTML, Ruby interactive reference (ri),
XML and Windows Help file (chm).

##
## ruby-ri
##
%package ri
Summary:    Ruby interactive reference
Group:      Documentation
Requires:   %{name} = %{version}-%{release}
Requires:   ruby(rdoc) = %{rubymmtver}
Provides:   ruby(ri) = %{rubymmver}
Provides:   ruby(ri) = %{rubymmtver}

%description ri
ri is a command line tool that displays descriptions of built-in
Ruby methods, classes and modules. For methods, it shows you the calling
sequence and a description. For classes and modules, it shows a synopsis
along with a list of the methods the class or module implements.

##
## ruby-static
##
%package static
Summary:    Static libraries for Ruby
Group:      Applications/System
Requires:   %{name} = %{version}-%{release}
Requires:   ruby(devel) = %{rubymmtver}

%description static
Static libraries for Ruby

##
## ruby-tcltk
##
%package tcltk
Summary:    Tcl/Tk interface for scripting language Ruby
Group:      Development/Languages
Requires:   %{name}-libs = %{version}-%{release}

%description tcltk
Tcl/Tk interface for the object-oriented scripting language Ruby.

%prep
%setup -q -n ruby-%{arcver}

%patch0 -p1
%patch1 -p1
%patch2 -p1

#%patch101 -p1
#%patch102 -p1
#%patch103 -p1
#%patch104 -p1
#%patch105 -p1

%patch201 -p1
%patch202 -p1
%patch203 -p1
%patch204 -p1
%patch205 -p1

%build
for i in config.sub config.guess; do
    test -f %{_datadir}/libtool/$i && cp %{_datadir}/libtool/$i .
done
autoconf

rb_cv_func_strtod=no
export rb_cv_func_strtod
CFLAGS="$RPM_OPT_FLAGS -fno-strict-aliasing"
export CFLAGS
%configure \
    --with-sitedir='%{ruby_sitelib}' \
    --with-sitearchdir='%{ruby_sitearch}' \
    --with-vendordir='%{ruby_vendorlib}' \
    --with-vendorarchdir='%{ruby_vendorarch}' \
    --program-suffix='%{?_compatsuffix}' \
    --with-default-kcode=none \
    --enable-shared \
    --enable-pthread \
    --disable-rpath \
    --with-ruby-version=full

make COPY="cp -p"

%install
rm -rf %{buildroot}

# installing binaries ...
make DESTDIR=%{buildroot} install

mkdir -p %{buildroot}/%{ruby_sitelib_186} \
        %{buildroot}/%{ruby_sitearch_186} \
        %{buildroot}/%{ruby_vendorlib_186} \
        %{buildroot}/%{ruby_vendorarch_186}

# generate ri doc
rm -rf .ext/rdoc
LD_LIBRARY_PATH=%{buildroot}%{_libdir} RUBYLIB=%{buildroot}%{ruby_vendorarch} make DESTDIR=%{buildroot} install-doc

# Find files with a shebang that do not have executable permissions
for script in `find %{buildroot}/%{ruby_sitelib} -type f ! -perm /a+x -name "*.rb"`; do
    [ ! -z "`head -n 1 $script | grep \"^#!/\"`" ] && chmod -v 755 $script
done

# Fix shebang
for script in `find %{buildroot}/%{ruby_sitelib} -type f -name "*.rb"`; do
    sed -r -i -e 's|(.*)/bin/ruby|%{_bindir}/ruby%{?_compatsuffix}|g' $script
done

if [ ! -d %{buildroot}/%{_includedir}/%{name}/ ]; then
    # The headers are probably in the vendor arch dir
    mkdir -p %{buildroot}/%{_includedir}/%{name}/
    find %{buildroot}/%{ruby_vendorarch_186} -mindepth 1 -maxdepth 1 -type f ! -name "*.so" ! -name "rbconfig.rb" -exec mv -v {} %{buildroot}/%{_includedir}/%{name}/. \;
fi

%clean
rm -rf %{buildroot}

%post
%{_sbindir}/alternatives --install %{_bindir}/ruby ruby %{_bindir}/ruby%{?_compatsuffix} 80 \
    --slave %{_bindir}/erb ruby-erb %{_bindir}/erb%{?_compatsuffix} \
    --slave %{_bindir}/testrb ruby-testrb %{_bindir}/testrb%{?_compatsuffix} \
    --slave %{_mandir}/man1/ruby.1.gz rubyman %{_mandir}/man1/ruby%{?_compatsuffix}.1.gz

%preun
if [ $1 = 0 ]; then
    %{_sbindir}/alternatives --remove ruby %{_bindir}/ruby%{?_compatsuffix}
fi

%post libs -p /sbin/ldconfig

%postun libs -p /sbin/ldconfig

%post rdoc
%{_sbindir}/alternatives --install %{_bindir}/rdoc ruby-rdoc %{_bindir}/rdoc%{?_compatsuffix} 90

%preun rdoc
if [ $1 = 0 ]; then
    %{_sbindir}/alternatives --remove ruby-rdoc %{_bindir}/rdoc%{?_compatsuffix}
fi

%files
%defattr(-, root, root, -)
%doc COPYING*
%doc ChangeLog
%doc GPL
%doc LEGAL
%doc LGPL
%doc NEWS
%doc README
%doc ToDo
%doc doc/ChangeLog-*
%doc doc/NEWS-*
%{_bindir}/erb%{?_compatsuffix}
%{_bindir}/ruby%{?_compatsuffix}
%{_bindir}/testrb%{?_compatsuffix}
%{_mandir}/man1/ruby%{?_compatsuffix}.1*

%files devel
%defattr(-, root, root, -)
%doc COPYING*
%doc ChangeLog
%doc GPL
%doc LEGAL
%doc LGPL
%doc README.EXT
%dir %{_includedir}/ruby%{?_compatsuffix}
%{_includedir}/ruby%{?_compatsuffix}/*.h
%{_libdir}/libruby%{?_compatsuffix}.so

%files irb
%defattr(-, root, root, -)
%{_bindir}/irb%{?_compatsuffix}
%{ruby_vendorlib_186}/irb.rb
%{ruby_vendorlib_186}/irb

%files libs
%defattr(-, root, root, -)
%doc README
%doc COPYING*
%doc ChangeLog
%doc GPL
%doc LEGAL
%doc LGPL
%dir %{_datadir}/ruby
%dir %{ruby_sitelib_186}
%dir %{ruby_sitearch_186}
%dir %{ruby_vendorlib_186}
%{ruby_vendorarch_186}
# The following files should go into the ruby-irb package.
%exclude %{ruby_vendorlib_186}/irb.rb
%exclude %{ruby_vendorlib_186}/irb
# The following files should go into the ruby-rdoc package.
%exclude %{ruby_vendorlib_186}/rdoc
# The following files should go into the ruby-tcltk package.
%exclude %{ruby_vendorlib_186}/*tk.rb
%exclude %{ruby_vendorlib_186}/tcltk.rb
%exclude %{ruby_vendorlib_186}/tk
%exclude %{ruby_vendorlib_186}/tk*.rb
%exclude %{ruby_vendorlib_186}/tkextlib
%exclude %{ruby_vendorarch_186}/tcltklib.so
%exclude %{ruby_vendorarch_186}/tkutil.so
# files in ruby-libs from here
%{ruby_vendorlib_186}/*.rb
%{ruby_vendorlib_186}/bigdecimal
%{ruby_vendorlib_186}/cgi
%{ruby_vendorlib_186}/date
%{ruby_vendorlib_186}/digest
%{ruby_vendorlib_186}/dl
%{ruby_vendorlib_186}/drb
%{ruby_vendorlib_186}/io
%{ruby_vendorlib_186}/net
%{ruby_vendorlib_186}/openssl
%{ruby_vendorlib_186}/optparse
%{ruby_vendorlib_186}/racc
%{ruby_vendorlib_186}/rexml
%{ruby_vendorlib_186}/rinda
%{ruby_vendorlib_186}/rss
%{ruby_vendorlib_186}/runit
%{ruby_vendorlib_186}/shell
%{ruby_vendorlib_186}/soap
%{ruby_vendorlib_186}/test
%{ruby_vendorlib_186}/uri
%{ruby_vendorlib_186}/webrick
%{ruby_vendorlib_186}/wsdl
%{ruby_vendorlib_186}/xmlrpc
%{ruby_vendorlib_186}/xsd
%{ruby_vendorlib_186}/yaml
%{_libdir}/libruby%{?_compatsuffix}.so.*

%files mode
%defattr(-, root, root, -)
#%{_datadir}/emacs/site-lisp/ruby-mode
#%{_datadir}/emacs/site-lisp/site-start.d/ruby-mode-init.el

%files rdoc
%defattr(-, root, root, -)
%{_bindir}/rdoc%{?_compatsuffix}
%{ruby_vendorlib_186}/rdoc

%files ri
%defattr(-, root, root, -)
%{_bindir}/ri%{?_compatsuffix}
%{_datadir}/ri/*

%files static
%defattr(-, root, root, -)
%{_libdir}/libruby%{?_compatsuffix}-static.a

%files tcltk
%defattr(-, root, root, -)
%{ruby_vendorlib_186}/*-tk.rb
%{ruby_vendorlib_186}/tcltk.rb
%{ruby_vendorlib_186}/tk
%{ruby_vendorlib_186}/tk*.rb
%{ruby_vendorlib_186}/tkextlib
%{ruby_vendorarch_186}/tcltklib.so
%{ruby_vendorarch_186}/tkutil.so

%changelog
* Tue Jan 19 2010 Jeroen van Meeuwen <j.van.meeuwen@ogd.nl> - 1.8.6-388-1
- New upstream patchlevel update

* Mon Jan 11 2010 Jeroen van Meeuwen <j.van.meeuwen@ogd.nl> - 1.8.6-383-13
- Fix the install scripts

* Sat Jan  2 2010 Jeroen van Meeuwen <j.van.meeuwen@ogd.nl> - 1.8.6-383-12
- Header files should be in %%{_includedir}

* Mon Dec 28 2009 Jeroen van Meeuwen <j.van.meeuwen@ogd.nl> - 1.8.6-383-11
- Align packaging with ruby-1.9.1

* Mon Dec 21 2009 Jeroen van Meeuwen <j.van.meeuwen@ogd.nl> - 1.8.6-383-6
- New upstream version
