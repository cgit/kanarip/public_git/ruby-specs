# General Ruby package locations

# This is the local lib/arch and should not be used for packaging
%ruby_sitelib %{_prefix}/local/share/ruby/
%ruby_sitearch %{_prefix}/local/%{_lib}/ruby/

# This is the general location for libs/archs compatible with all
# or most of the Ruby versions available in the Fedora repositories
%ruby_vendorlib %{_datadir}/ruby/
%ruby_vendorarch %{_libdir}/ruby/

# This are more specific
%ruby_sitelib_192 %{_prefix}/local/share/ruby/1.9.2/
%ruby_sitelib_191 %{_prefix}/local/share/ruby/1.9.1/
%ruby_sitelib_19 %{_prefix}/local/share/ruby/1.9/

%ruby_sitelib_186 %{_prefix}/local/share/ruby/1.8.6/
%ruby_sitelib_185 %{_prefix}/local/share/ruby/1.8.5/
%ruby_sitelib_18 %{_prefix}/local/share/ruby/1.8/

%ruby_sitearch_192 %{_prefix}/local/%{_lib}/ruby/1.9.2/
%ruby_sitearch_191 %{_prefix}/local/%{_lib}/ruby/1.9.1/
%ruby_sitearch_19 %{_prefix}/local/%{_lib}/ruby/1.9/

%ruby_sitearch_186 %{_prefix}/local/%{_lib}/ruby/1.8.6/
%ruby_sitearch_185 %{_prefix}/local/%{_lib}/ruby/1.8.5/
%ruby_sitearch_18 %{_prefix}/local/%{_lib}/ruby/1.8/

%ruby_vendorlib_192 %{_datadir}/ruby/1.9.2/
%ruby_vendorlib_191 %{_datadir}/ruby/1.9.1/
%ruby_vendorlib_19 %{_datadir}/ruby/1.9/

%ruby_vendorlib_186 %{_datadir}/ruby/1.8.6/
%ruby_vendorlib_185 %{_datadir}/ruby/1.8.5/
%ruby_vendorlib_18 %{_datadir}/ruby/1.8/

%ruby_vendorarch_192 %{_libdir}/ruby/1.9.2/
%ruby_vendorarch_191 %{_libdir}/ruby/1.9.1/
%ruby_vendorarch_19 %{_libdir}/ruby/1.9/

%ruby_vendorarch_186 %{_libdir}/ruby/1.8.6/
%ruby_vendorarch_185 %{_libdir}/ruby/1.8.5/
%ruby_vendorarch_18 %{_libdir}/ruby/1.8/

